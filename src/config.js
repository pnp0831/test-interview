module.exports = {
  env: (typeof window !== 'undefined' ? window.ENV : process.env.NODE_ENV) || 'development',
  app: {
    title: 'Home test | Phat',
    head: {
      meta: [
        {
          name: 'description',
          content: 'Home test',
        },
      ],
    },
  },
};
