import NextRoutes from 'next-routes';
import { ROUTE_NAME, PATTERN_URL, PAGE_NAME } from '~/constants/routes';

const routes = new NextRoutes();

// http://localhost:3009/
routes.add({
  name: ROUTE_NAME.HOME,
  pattern: PATTERN_URL.HOME,
  page: PAGE_NAME.HOME,
});

export default routes;
