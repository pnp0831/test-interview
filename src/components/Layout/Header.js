import React, { memo } from 'react';
import AppsIcon from '@material-ui/icons/Apps';
import Avatar from '@material-ui/core/Avatar';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import './style.scss';
import Container from '@material-ui/core/Container';

const Header = memo(() => (
  <div className="wrapper-header">
    <Container maxWidth="lg">
      <div className="wrapper-header-main flex flex--stretch">
        <div className="flex wrapper-header-service">
          <AppsIcon />
          <span>All Services</span>
        </div>
        <div className="flex wrapper-header-user">
          <Avatar src="/avatar.png" alt="avatar" />
          <span>howdycandidate</span>
          <ArrowDropDownIcon />
        </div>
      </div>
    </Container>
  </div>
));

export default Header;
