import { checkIsMobile } from '~/helpers';

const serverProps = (context) => {
  const { req } = context;
  const userAgent = req.headers['user-agent'];
  const isMobile = checkIsMobile(userAgent);
  return {
    isMobile,
  };
};

const clientProps = () => {
  const { userAgent } = window.navigator;
  const isMobile = checkIsMobile(userAgent);

  return {
    isMobile,
  };
};

const initProps = (context) => {
  const isServer = typeof window === 'undefined';
  const initAppProps = isServer ? serverProps : clientProps;
  return initAppProps(context);
};

export default initProps;
