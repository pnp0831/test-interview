/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '~/theme';
import { app } from '~/config';
import getInitProps from '~/helpers/initProps';
import WithLayout from '~/hocs/withLayout';
import '~/assets/global.scss';

const renderAppleMetas = () => app.head.meta.map((item, i) => <meta {...item} key={i} />);

const MyApp = ({ Component, appProps = {}, pageProps = {} }) => {
  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);
  return (
    <>
      <Head>
        <title>{app.title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        {renderAppleMetas()}
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <WithLayout>
          <Component {...appProps} {...pageProps} />
        </WithLayout>
      </ThemeProvider>
    </>
  );
};

MyApp.getInitialProps = async ({ Component, ctx }) => {
  const appProps = getInitProps(ctx);
  const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};

  return {
    pageProps,
    appProps,
  };
};

MyApp.propTypes = {
  appProps: PropTypes.object,
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};

export default MyApp;
