import React, { memo } from 'react';
import HomePageComponent from '~/features/HomePage';

const HomePage = () => <HomePageComponent />;

export default memo(HomePage);
