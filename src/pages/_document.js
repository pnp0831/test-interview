import React from 'react';
import { ServerStyleSheets } from '@material-ui/styles';
import Document, { Main, NextScript, Html, Head } from 'next/document';

export default class ServerDocument extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheets();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          // eslint-disable-next-line react/jsx-props-no-spreading
          enhanceApp: (App) => (props) => sheet.collect(<App {...props} />),
        });

      const documentProps = await Document.getInitialProps(ctx);
      return {
        ...documentProps,
        styles: (
          <>
            {documentProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      ctx.renderPage(sheet);
    }
  }

  render() {
    return (
      <Html lang="vi">
        <Head />
        <body>
          {this.props.styleTags}
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
