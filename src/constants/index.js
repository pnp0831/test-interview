export const MAIN_INFOMATION = [
  {
    text: 'User name',
    value: 'howdycandidate',
    isIcon: true,
    type: 'text',
  },
  {
    text: 'Firstname',
    value: 'howdycandidate',
    isIcon: true,
    type: 'text',
  },
  {
    text: 'Last name',
    value: 'pewpew',
    isIcon: true,
    type: 'text',
  },
  {
    text: 'Date of Birth',
    value: '1999-10-12',
    isIcon: true,
    type: 'date',
  },
  {
    text: 'Password',
    value: 'Change password',
    isUnderline: true,
    type: 'link',
  },
  {
    text: 'Country',
    value: 'Chile',
    isIcon: true,
    type: 'text',
  },
  {
    text: 'Currency',
    value: 'THB',
    isIcon: true,
    type: 'text',
  },
  {
    text: 'Email',
    value: 'email@example.com',
    isIcon: true,
    type: 'text',
  },
  {
    text: 'Gender',
    type: 'select',
  },
];
