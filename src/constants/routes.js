const ROUTE_NAME = {
  HOME: '#homeView',
};

const PAGE_NAME = {
  HOME: 'index',
};

const PATTERN_URL = {
  HOME: '/',
};

const ROUTE_PARAMS = [];

export { ROUTE_NAME, PAGE_NAME, PATTERN_URL, ROUTE_PARAMS };
