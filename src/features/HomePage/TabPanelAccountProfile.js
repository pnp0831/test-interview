/* eslint-disable react/no-array-index-key */
import React, { memo, useState } from 'react';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Lock from '@material-ui/icons/Lock';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { MAIN_INFOMATION } from '~/constants';
import './style.scss';

const useStyles = makeStyles((theme) => ({
  formControl: {
    width: '100%',
    minWidth: 'auto',

    '&:before': {
      borderBottom: 'none !important',
    },
    '&:after': {
      borderBottom: 'none !important',
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  underline: {
    '&:before': {
      borderBottom: 'none',
    },
    '&:after': {
      borderBottom: 'none',
    },
  },

  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    whiteSpace: 'nowrap',
    marginBottom: theme.spacing(1),
  },
  divider: {
    margin: theme.spacing(2, 0),
  },
}));

const TabPanelAccountProfile = memo(() => {
  const classes = useStyles();

  const [phoneNumber, setPhoneNumber] = useState('*****6679');

  return (
    <div className="wrapper-tab-account">
      <Grid container spacing={3}>
        <Grid container item lg={6} md={6} sm={12}>
          <div className="wrapper-avatar">
            <img src="/main-avatar.png" loading="lazy" alt="avatar" />
            <div className="wrapper-avatar-title">Howdycandidate</div>
            <div className="wrapper-avatar-des">THB 565.000</div>
          </div>
          <div className="wrapper-form">
            <div className="wrapper-form-title">Communication Details</div>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel htmlFor="mobile-number" shrink>
                Mobile Number
              </InputLabel>
              <Input
                disableUnderline
                name="mobile-number"
                placeholder="Mobile Number"
                endAdornment={
                  <InputAdornment
                    className="custom-text"
                    position="end"
                    style={{ position: 'absolute', right: '1rem' }}
                  >
                    <Button variant="text" color="primary" disableRipple>
                      Verify
                    </Button>
                  </InputAdornment>
                }
                onChange={(e) => {
                  let { value } = e.target;

                  value = value.replace(/\d(?=\d{4})/g, '*');

                  setPhoneNumber(value);
                }}
                value={phoneNumber}
              />
            </FormControl>
            <FormControl fullWidth className={classes.formControl} style={{ marginTop: '14px' }}>
              <InputLabel htmlFor="age-native-simple" shrink>
                Language
              </InputLabel>
              <Select native disableUnderline>
                {['Singapore', 'Vietnamese'].map((name) => (
                  <option key={name} value={name}>
                    {name}
                  </option>
                ))}
              </Select>
            </FormControl>
          </div>
        </Grid>
        <Grid container item lg={6} md={6} sm={12}>
          <div className="wrapper-main">
            <div className="wrapper-main-title">Account Details</div>
            {MAIN_INFOMATION.map(({ text, value, isIcon, isUnderline, type }, index) => {
              const isHasBG = index % 2 === 0;

              return (
                <div
                  key={index}
                  className={`flex flex--stretch wrapper-main-info ${
                    isHasBG ? 'has-background' : ''
                  }`}
                >
                  <div className="flex">
                    <div className="text">{text}</div>
                    <div className="colon">:</div>
                    {value && (
                      <div
                        style={{
                          textDecoration: isUnderline ? 'underline' : 'none',
                          cursor: 'pointer',
                        }}
                      >
                        {type === 'text' && <Input disableUnderline defaultValue={value} />}
                        {type === 'date' && (
                          <Input type="date" disableUnderline defaultValue={value} />
                        )}
                        {type === 'link' && value}
                      </div>
                    )}
                  </div>
                  {type === 'select' && (
                    <Select native disableUnderline>
                      {['Male', 'Female'].map((name) => (
                        <option key={name} value={name}>
                          {name}
                        </option>
                      ))}
                    </Select>
                  )}
                  {isIcon && <Lock />}
                </div>
              );
            })}
          </div>
        </Grid>
      </Grid>

      <div className="wrapper-form pt-40" style={{ marginTop: '26px' }}>
        <Grid container spacing={3} direction="row">
          <Grid item lg={6} md={6} sm={12}>
            <div className="wrapper-form-title">Home address</div>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="address" shrink>
                Address
              </InputLabel>
              <Input
                name="address"
                fullWidth
                multiline
                disableUnderline
                rowsMax={4}
                rows={4}
                style={{ padding: 0 }}
                defaultValue="Address"
              />
            </FormControl>
          </Grid>
          <Grid container item lg={6} md={6} sm={12} direction="column" justify="space-between">
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="town-city" shrink>
                Town/City
              </InputLabel>
              <Input disableUnderline name="town-city" fullWidth defaultValue="Town/City" />
            </FormControl>
            <FormControl className={classes.formControl} style={{ marginTop: '14px' }}>
              <InputLabel htmlFor="postal" shrink>
                Postal Code
              </InputLabel>
              <Input disableUnderline name="postal" fullWidth defaultValue="000000" />
            </FormControl>
          </Grid>
        </Grid>
      </div>

      <div className="flex flex--center wrapper-submit text-center">
        <div className="wrapper-submit-question">
          Would you like to get the latest promos, updates, and offers?
        </div>
        <FormControlLabel
          control={<Checkbox color="primary" />}
          label="Yes. Send me the latest promotions, updates and offers."
        />
        <div className="flex flex--center">
          <Button className="btn btn-save">Save change</Button>
          <Button className="btn btn-cancel">Cancel</Button>
        </div>
      </div>
    </div>
  );
});

TabPanelAccountProfile.propTypes = {};

export default TabPanelAccountProfile;
