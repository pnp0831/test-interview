/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { memo } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonIcon from '@material-ui/icons/Person';
import MailIcon from '@material-ui/icons/Mail';
import TabPanelAccountProfile from './TabPanelAccountProfile';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      key={index}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const Home = memo(() => (
  <div className="wrapper-profile">
    <AppBar position="static">
      <Tabs value={0} className="wrapper-tabs" variant="scrollable" scrollButtons="auto">
        <Tab
          label={
            <div className="flex">
              <PersonIcon style={{ marginRight: '10px' }} />
              Account profile
            </div>
          }
          {...a11yProps(0)}
        />
        <Tab
          label={
            <div className="flex">
              <AccountBalanceIcon style={{ marginRight: '10px' }} />
              Update Bank Detail
            </div>
          }
          {...a11yProps(1)}
        />
        <Tab
          label={
            <div className="flex">
              <MailIcon style={{ marginRight: '10px' }} />
              Inbox
            </div>
          }
          {...a11yProps(2)}
        />
      </Tabs>
    </AppBar>
    <TabPanel value={0} index={0}>
      <TabPanelAccountProfile />
    </TabPanel>
  </div>
));

Home.propTypes = {};

export default Home;
