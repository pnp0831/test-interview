import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Header from '~/components/Layout/Header';

const WithLayout = memo(({ children }) => (
  <>
    <Header />
    <div style={{ maxWidth: '1006px', margin: '120px auto', padding: '1rem' }}>{children}</div>
  </>
));

WithLayout.propTypes = {
  children: PropTypes.object.isRequired,
};

export default WithLayout;
